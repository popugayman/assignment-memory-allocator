#include "mem.h"


void test_success()
{
    _malloc(100);
    _malloc(100);
    _malloc(200);
    _malloc(300);
}

void test_free_one_block()
{
    _malloc(200);
    _malloc(1000);
    _malloc(20);
    void* a = _malloc(789);
    _malloc(123);
    _free(a);
}

void test_free_two_blocks()
{
    _malloc(200);
    void* b = _malloc(1000);
    _malloc(20);
    void* a = _malloc(789);
    _malloc(123);
    _free(a);
    _free(b);
}

void test_extends_by_new_region()
{
    _malloc(19000);
    _malloc(2000);

}

void test_extends_failed()
{
    _malloc(999999999);
    _malloc(100);
}
