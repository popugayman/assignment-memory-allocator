#include <stdio.h>
#include "mem_internals.h"
#include "mem.h"
#include "tests.h"

void free_heap (void *heap){
    struct block_header* header = heap;
    while(header)
    {
        _free(header->contents);
        header = header->next;
    }
    _free(((struct block_header*) heap)->contents);
}

int main() {

    void* heap = heap_init(20000);

    fprintf(stdout, "\nTest #1\n");
    test_success();
    debug_heap(stdout, heap);
    free_heap(heap);

    fprintf(stdout, "\nTest #2\n");
    test_free_one_block();
    debug_heap(stdout, heap);
    free_heap(heap);

    fprintf(stdout, "\nTest #3\n");
    test_free_two_blocks();
    debug_heap(stdout, heap);
    free_heap(heap);

    fprintf(stdout, "\nTest #4\n");
    test_extends_by_new_region();
    debug_heap(stdout, heap);
    free_heap(heap);

    fprintf(stdout, "\nTest #5\n");
    test_extends_failed();
    debug_heap(stdout, heap);
    free_heap(heap);

    return 0;
}
