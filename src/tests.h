#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

void test_success();
void test_free_one_block();
void test_free_two_blocks();
void test_extends_by_new_region();
void test_extends_failed();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
